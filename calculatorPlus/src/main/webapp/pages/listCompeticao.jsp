<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"> 
                <i class="nc-icon nc-trophy"></i>
                Minhas Competi��es</h4>
            <p class="category">Competi��es entre Alunos</p>
            <form action="?ac=competicao" method="POST">
                <input type="hidden" name="op" value="new"/>
                <button type="submit" class="btn btn-primary btn-round">Participar de nova competi��o</button>
            </form>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                        <tr><th>
                                Data
                            </th>
                            <th>
                                Bonifica��o
                            </th>
                            <th>
                                Acertos
                            </th>
                            <th >
                                Erros
                            </th>
                            <th class="text-right">
                                ...
                            </th>
                        </tr></thead>
                    <tbody>
                        <c:if test="${requestScope.competicoes.size() == 0}">
                            <tr>
                                <td colspan="5">
                                    N�o Participou de Competi��es, Participe agora...
                                </td>
                            </tr>
                        </c:if>
                        <c:forEach items="${requestScope.competicoes}" var="c" >
                            <tr>
                                <td>
                                    ${ c.dataFormatada}
                                </td>
                                <td>
                                    Cr$ ${c.bonificacao}
                                </td>
                                <td>
                                    ${c.acertos}
                                </td>
                                <td>
                                    ${c.erros}
                                </td>
                                <td class="text-right">
                                    <a href="?ac=detalhesPart&id=${c.id}" >
                                        <i class="nc-icon nc-alert-circle-i"></i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
