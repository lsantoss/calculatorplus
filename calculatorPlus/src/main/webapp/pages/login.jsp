<div class="col-md-6">
    <div class="card card-user">
        <div class="card-header">
            <h5 class="card-title">Login</h5>
        </div>
        <div class="card-body">
            <form action="?ac=login" method="POST">
                <input type="hidden" name="ac" value="login"/>
                <div class="row">
                    <div class="col-md-11 pr-1">
                        <div class="form-group">
                            <label>LOGIN</label>
                            <input type="text" class="form-control" name="cpLogin" required="true" placeholder="Login" 
                                   value="${requestScope.usr.nome}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 pr-1">
                        <div class="form-group">
                            <label>SENHA</label>
                            <input type="password" class="form-control" name="cpSenha" required="true" placeholder="Senha" 
                                   value="${requestScope.usr.email}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="update ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary btn-round">logar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
