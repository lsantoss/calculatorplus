           
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
        <div class="card-body ">
            <div class="row">
                <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                        <i class="nc-icon nc-globe text-warning"></i>
                    </div>
                </div>
                <div class="col-7 col-md-8">
                    <div class="numbers">
                        <p class="card-category">Usu�rios</p>
                        <p class="card-title">${requestScope.numeroUsers}
                        <p>
                    </div>
                </div>
            </div>
        </div>
        <!--              <div class="card-footer ">
                        <hr>
                        <div class="stats">
                          <i class="fa fa-refresh"></i> Update Now
                        </div>
                      </div>-->
    </div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
        <div class="card-body ">
            <div class="row">
                <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                        <i class="nc-icon nc-money-coins text-success"></i>
                    </div>
                </div>
                <div class="col-7 col-md-8">
                    <div class="numbers">
                        <p class="card-category">Premia��es</p>
                        <p class="card-title">Cr$ ${requestScope.premiacaoUsers}
                        <p>
                    </div>
                </div>
            </div>
        </div>
        <!--              <div class="card-footer ">
                        <hr>
                        <div class="stats">
                          <i class="fa fa-calendar-o"></i> Last day
                        </div>
                      </div>-->
    </div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
        <div class="card-body ">
            <div class="row">
                <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                        <i class="nc-icon nc-tap-01 text-danger"></i>
                    </div>
                </div>
                <div class="col-7 col-md-8">
                    <div class="numbers">
                        <p class="card-category">Erros</p>
                        <p class="card-title">${requestScope.numeroErros}
                        <p>
                    </div>
                </div>
            </div>
        </div>
        <!--              <div class="card-footer ">
                        <hr>
                        <div class="stats">
                          <i class="fa fa-clock-o"></i> In the last hour
                        </div>
                      </div>-->
    </div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-stats">
        <div class="card-body ">
            <div class="row">
                <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                        <i class="nc-icon nc-favourite-28 text-primary"></i>
                    </div>
                </div>
                <div class="col-7 col-md-8">
                    <div class="numbers">
                        <p class="card-category">Acertos</p>
                        <p class="card-title">${requestScope.numeroAcertos}
                        <p>
                    </div>
                </div>
            </div>
        </div>
        <!--              <div class="card-footer ">
                        <hr>
                        <div class="stats">
                          <i class="fa fa-refresh"></i> Update now
                        </div>
                      </div>-->
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <c:set var="graph" value="ok" scope="request"/>
                <h5 class="card-title">Comportamento dos Usu�rios</h5>
                <p class="card-category">�ltimos 10 dias</p>
            </div>
            <div class="card-body ">
                <canvas id=chartHours width="400" height="100"></canvas>
            </div>
            <div class="card-footer ">
                <hr>
                <div class="stats">
                    <i class="fa fa-history"></i> atualizado as <span id="timeUpdate">00:00</span>
                </div>
            </div>
        </div>
    </div>
</div>
