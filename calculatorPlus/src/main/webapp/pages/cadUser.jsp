<div class="col-md-12">
    <div class="card card-user">
        <div class="card-header">
            <h5 class="card-title">Perfil do Usu�rio</h5>
        </div>
        <div class="card-body">
            <form action="?ac=saveUser" method="POST">
                <input type="hidden" name="ac" value="saveUser"/>
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="cpNome" required="true" placeholder="Nome" value="${requestScope.usr.nome}">
                        </div>
                    </div>
                    <div class="col-md-3 px-1">
                        <div class="form-group">
                            <label>Login</label>
                            <input type="text" class="form-control" name="cpLogin" required="true" minlength="4" maxlength="15" placeholder="Login" value="${requestScope.usr.login}">
                        </div>
                    </div>
                    <div class="col-md-3 pl-1">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Senha</label>
                            <input type="password" class="form-control"  minlength="6" name="cpSenha" required="true" placeholder="Senha">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label>email</label>
                            <input type="email" class="form-control" name="cpEmail" required="true" placeholder="Email" value="${requestScope.usr.email}">
                        </div>
                    </div>
                    <div class="col-md-6 pl-1">
                        <div class="form-group">
                            <label>Cidade</label>
                            <input type="text" class="form-control" name="cpCidade" required="true" placeholder="Cidade" value="${requestScope.usr.cidade}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Data de Nascimento</label>
                            <input type="date" class="form-control" name="cpData" required="true" placeholder="Cidade" value="${requestScope.usr.data}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="update ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary btn-round">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
