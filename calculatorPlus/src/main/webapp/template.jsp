<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--
=========================================================
 Paper Dashboard 2 - v2.0.0
=========================================================

 Product Page: https://www.creative-tim.com/product/paper-dashboard-2
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE)

 Coded by Creative Tim

=========================================================

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->



<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="./assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Calculator Dashboard by Tabajara
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <!-- CSS Files -->
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="./assets/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="./assets/demo/demo.css" rel="stylesheet" />
        <link href="./assets/css/newcss.css" rel="stylesheet" />
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-color="white" data-active-color="danger">
                <!--
                  Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
                -->
                <div class="logo">
                    <a href="?ac=dashboard" class="simple-text logo-mini">
                        <div class="logo-image-small">
                            <img src="./assets/img/logo-small.png">
                        </div>
                    </a>
                    <a href="?ac=dashboard" class="simple-text logo-normal">
                        Calculator 
                        <!-- <div class="logo-image-big">
                          <img src="./assets/img/logo-big.png">
                        </div> -->
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="${param.ac=='dashboard'?'active':''}">
                            <a href="?ac=dashboard">
                                <i class="nc-icon nc-bank"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <c:if test="${sessionScope.user == null}">
                            <li class="${param.ac=='login'?'active':''}">
                                <a href="?ac=login">
                                    <i class="nc-icon nc-circle-10"></i>
                                    <p>Login</p>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${sessionScope.user == null}">
                            <li  class="${param.ac=='cadastro'?'active':''}">
                                <a href="?ac=cadastro">
                                    <i class="nc-icon nc-single-02"></i>
                                    <p>Cadastro</p>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${sessionScope.user != null}">
                            <li class="${param.ac=='competicao'?'active':''}">
                                <a href="?ac=competicao">
                                    <i class="nc-icon nc-user-run"></i>
                                    <p>Competi��o</p>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${sessionScope.user != null}">
                            <li class="${param.ac=='ranking'?'active':''}">
                                <a href="?ac=ranking">
                                    <i class="nc-icon nc-diamond"></i>
                                    <p>Ranking</p>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${sessionScope.user != null}">
                            <li>
                                <a href="?ac=logout">
                                    <i class="nc-icon nc-share-66"></i>
                                    <p>Sair</p>
                                </a>
                            </li>
                        </c:if>

                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="#">

                                <c:if test="${sessionScope.user == null}">
                                    Crie seu cadastro 
                                </c:if>                                    
                                <c:if test="${sessionScope.user != null}">
                                    Bem Vindo, ${sessionScope.user.nome}
                                </c:if>

                            </a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                        </button>

                    </div>
                </nav>
                <!-- End Navbar -->
                <!-- <div class="panel-header panel-header-lg">
          
            <canvas id="bigDashboardChart"></canvas>
          
          
          </div> -->
                <div class="content">
                    <div class="row">

                        <c:if test="${requestScope.msgAviso != null}">
                            <!--                            <div class="alert alert-info alert-with-icon alert-dismissible fade show" data-notify="container">
                                                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="nc-icon nc-simple-remove"></i>
                                                            </button>
                                                            <span data-notify="icon" class="nc-icon nc-bell-55"></span>
                                                            <span data-notify="message">${requestScope.msgAviso}</span>
                                                        </div>-->
                        </c:if>

                        <c:catch var="erro">
                            <c:if test="${param.page == null}">
                                <jsp:include page="/pages/home.jsp" />
                            </c:if>
                            <c:if test="${param.page != null}">
                                <jsp:include page="/pages/${param.page}.jsp" />
                            </c:if>
                        </c:catch>
                        <c:if test="${erro != null}">
                            <h1>Aconteceu Algo errado!!!!</h1> 
                            <br />
                            <h3>A��o "${param.page}" n�o encontrada </h3>
                        </c:if>

                    </div>
                </div>
                <footer class="footer footer-black  footer-white ">
                    <div class="container-fluid">
                        <div class="row">
                            <nav class="footer-nav">
                                <ul>
                                    <li>
                                        <a href="#" >Teste de Software</a>
                                    </li>
                                </ul>
                            </nav>
                            <div class="credits ml-auto">
                                <span class="copyright">
                                    ©
                                    <script>
                                        document.write(new Date().getFullYear())
                                    </script>, feito <i class="fa fa-coffee coffee  "></i> por Daves Martins
                                </span>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="assets/js/core/jquery.min.js"></script>
        <script src="assets/js/core/popper.min.js"></script>
        <script src="assets/js/core/bootstrap.min.js"></script>
        <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!--  Google Maps Plugin    -->
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>-->
        <!-- Chart JS -->
        <script src="assets/js/plugins/chartjs.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="assets/js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="assets/js/paper-dashboard.min.js?v=2.0.0" type="text/javascript"></script>
        <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
        <script src="assets/demo/demo.js"></script>
        <script>
                                        $(document).ready(function () {
                                            // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
                                            //demo.initChartsPages();
            <c:if test="${requestScope.msgErro != null}">
                                            $.notify({
                                                title: "<strong>ERRO ::</strong>",
                                                message: '${requestScope.msgErro}',
                                                icon: "nc-icon nc-bell-55"
                                            }, {
                                                // settings
                                                type: 'danger',
                                                placement: {
                                                    from: 'top',
                                                    align: 'center'
                                                },
                                                animate: {
                                                    enter: 'animated bounceInDown',
                                                    exit: 'animated bounceOutUp'
                                                },
                                                delay: 5000
                                            });
            </c:if>
            <c:if test="${requestScope.msgAviso != null}">
                                            $.notify({
                                                title: "<strong>AVISO ::</strong>",
                                                message: '${requestScope.msgAviso}',
                                                icon: "nc-icon nc-bell-55"
                                            }, {
                                                // settings
                                                type: 'info',
                                                showProgressbar: true,
                                                delay: 5000
                                            });
            </c:if>

            <c:if test="${graph != null && graph == 'ok'}">
                                            dataAjax = function () {
                                                //alert("oi");
                                                //Esta � a fun��o Ajax// Seu inicio � por essa sintaxe
                                                $.ajax
                                                        ({
                                                            type: 'POST', //Esta propriedade diz que tipo de intera��o faremos entre os dados, neste caso por POST
                                                            dataType: 'json', //Tipo de retorno que tera o ajax, neste caso retornaremos um tipo html, p�gina em html
                                                            url: '?ac=ajaxData', //Qual p�gina o ajax ira buscar 
//                                                            data: valores, //Este s�o os valores do POST do formularios, neste caso as datas
                                                            success: function (retorno) //Em caso de sucesso da fun��o .ajax ele retornara para a var retorno
                                                            {
                                                                //alert(retorno);
                                                                var data = new Date();
                                                                demo.initChartsPages(retorno);
                                                                $("#timeUpdate").html(data.getHours().toString().padStart(2, '0') + ":" + data.getMinutes().toString().padStart(2, '0'));
//                                                                loading_hide(); //Fun��o que esconde novamente a imagem do gif de loading
//                                                                $('#resultados').html(retorno).fadeIn(); //Aqui estamos inserido o conteudo de retorno na local indicado    
                                                            }
                                                        });
                                            }

                                            dataAjax();
                                            setInterval(dataAjax, 500000);
            </c:if>
                                        });
        </script>
    </body>

</html>

