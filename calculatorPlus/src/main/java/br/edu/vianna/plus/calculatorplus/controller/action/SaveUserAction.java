/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.action;

import br.edu.vianna.plus.calculatorplus.controller.faces.IActionCommand;
import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import br.edu.vianna.plus.calculatorplus.util.Util;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daves
 */
public class SaveUserAction implements IActionCommand {

    public SaveUserAction() {
    }

    @Override
    public boolean ehSegura() {
        return false;
    }

    
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
            Usuario user = new Usuario(null, request.getParameter("cpNome"),
                    request.getParameter("cpLogin"), request.getParameter("cpEmail"),
                    request.getParameter("cpSenha"), request.getParameter("cpCidade"),
                    new Date());
        try {
            
            user.setDataNascimento(Util.toDate(request.getParameter("cpData")));
            
            if (!user.senhaValida()){
                throw new Exception("A senha deve possuir no minímo 6 digitos, sendo "
                        + "letras maiúsculas, minúsculas, "
                        + "números e caracteres especiais.");
            }
            
            if (!user.loginValida()){
                throw new Exception("Login deve ter de 6 a 15 caractéres, sem espaço");
            }
            
            new UsuarioDao().save(user);
            request.setAttribute("msgAviso", "Salvo com Sucesso!!!");
            new ViewLoginAction().execute(request, response);
            
        } catch (ParseException e) {
            request.setAttribute("msgAviso", "Data com formato errado :: informe dd/mm/yyyy");
            request.setAttribute("usr", user);
            new ViewCadastroUserAction().execute(request, response);
        } catch (Exception e) {
            request.setAttribute("msgAviso", e.getMessage());
            request.setAttribute("usr", user);
            new ViewCadastroUserAction().execute(request, response);
        }

    }

}
