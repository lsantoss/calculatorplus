/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.action;

import br.edu.vianna.plus.calculatorplus.controller.faces.IActionCommand;
import br.edu.vianna.plus.calculatorplus.dao.JogoDao;
import com.google.gson.Gson;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daves
 */
public class ajaxDataGraphAction implements IActionCommand {

    @Override
    public boolean ehSegura() {
        return false;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        Gson gson = new Gson();
        Random r = new Random(new Date().getTime());
        long[][] vetor = new long[2][10];
        vetor[1] = new JogoDao().getAcertosPorDia();
        vetor[0] = new JogoDao().getErrosPorDia();
//        for (int i = 0; i < vetor[0].length; i++) {
//             vetor[0][i] = r.nextInt(200);
//             vetor[1][i] = r.nextInt(600);
//        }
        
        out.print(gson.toJson(vetor));
        out.flush();
    }

}
