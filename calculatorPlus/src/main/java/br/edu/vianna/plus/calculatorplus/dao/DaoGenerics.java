/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author daves
 */
public class DaoGenerics {

    protected EntityManager conection;

    public DaoGenerics() {

        conection = getInstancia();
    }

    public EntityManager getConection() {
        return conection;
    }

    private static EntityManager instancia;

    synchronized public static EntityManager getInstancia() {
        if (instancia == null) {
            instancia = Persistence.createEntityManagerFactory("PUcalculatorPlus").createEntityManager();
        }
        return instancia;
    }

}
