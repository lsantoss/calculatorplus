/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.domain;

import br.edu.vianna.plus.calculatorplus.dao.JogoDao;
import br.edu.vianna.plus.calculatorplus.util.Util;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author daves
 */
@Entity
@Table(catalog = "calculator", schema = "", name = "jogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jogo.findAll", query = "SELECT j FROM Jogo j")
    , @NamedQuery(name = "Jogo.findByIdjogo", query = "SELECT j FROM Jogo j WHERE j.idjogo = :idjogo")
    , @NamedQuery(name = "Jogo.findByValor1", query = "SELECT j FROM Jogo j WHERE j.valor1 = :valor1")
    , @NamedQuery(name = "Jogo.findByValor2", query = "SELECT j FROM Jogo j WHERE j.valor2 = :valor2")
    , @NamedQuery(name = "Jogo.findByOperador", query = "SELECT j FROM Jogo j WHERE j.operador = :operador")
    , @NamedQuery(name = "Jogo.findByResultado", query = "SELECT j FROM Jogo j WHERE j.resultado = :resultado")
    , @NamedQuery(name = "Jogo.findByResposta", query = "SELECT j FROM Jogo j WHERE j.resposta = :resposta")})
public class Jogo implements Serializable {

    private static final long serialVersionUID = 1L;

    public static List<Jogo> criarJogosAleatorio(Partida part) {
        return criarJogosAleatorio(part, 10);
    }

    public static List<Jogo> criarJogosAleatorio(Partida part, int idx) {
        List<Jogo> jogos = new ArrayList<>();
        Random rand = new Random(new Date().getTime());
        for (int j = 0; j < idx; j++) {
            final int v1 = rand.nextInt(50);
            final int v2 = rand.nextInt(50);
            final int pos = rand.nextInt(4);
            final EOperator op = EOperator.values()[pos];
            Jogo jogo = new Jogo(null, v1, v2, op, op.calcular(v1, v2), -1111, 0);
            jogo.setPartida(part);
            jogo.setBonus(jogo.bonusInicial().doubleValue());
            jogos.add(jogo);
        }
        return jogos;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idjogo;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double valor1;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double valor2;
    @Basic(optional = false)
    @NotNull
//    @Size(min = 1, max = 2)
//    @Column(nullable = false, length = 2)
//    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('+', '-', '/','*')")
    @Enumerated(EnumType.STRING)
    private EOperator operador;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double resultado;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double resposta;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double bonus;
    @JoinColumn(name = "partida", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Partida partida;

    public Jogo() {
    }

    public Jogo(Integer idjogo) {
        this.idjogo = idjogo;
    }

    public Jogo(Integer idjogo, double valor1, double valor2, EOperator operador, double resultado, double resposta, double bonus) {
        this.idjogo = idjogo;
        this.valor1 = valor1;
        this.valor2 = valor2;
        this.operador = operador;
        this.resultado = resultado;
        this.resposta = resposta;
        this.bonus = bonus;
    }

    public Integer getIdjogo() {
        return idjogo;
    }

    public void setIdjogo(Integer idjogo) {
        this.idjogo = idjogo;
    }

    public double getValor1() {
        return valor1;
    }

    public void setValor1(double valor1) {
        this.valor1 = valor1;
    }

    public double getValor2() {
        return valor2;
    }

    public void setValor2(double valor2) {
        this.valor2 = valor2;
    }

    public EOperator getOperador() {
        return operador;
    }

    public void setOperador(EOperator operador) {
        this.operador = operador;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }

    public double getResposta() {
        return resposta;
    }

    public void setResposta(double resposta) {
        this.resposta = resposta;
    }

    public Partida getPartida() {
        return partida;
    }

    public void setPartida(Partida partida) {
        this.partida = partida;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.idjogo);
        hash = 61 * hash + (int) (Double.doubleToLongBits(this.valor1) ^ (Double.doubleToLongBits(this.valor1) >>> 32));
        hash = 61 * hash + (int) (Double.doubleToLongBits(this.valor2) ^ (Double.doubleToLongBits(this.valor2) >>> 32));
        hash = 61 * hash + Objects.hashCode(this.operador);
        hash = 61 * hash + (int) (Double.doubleToLongBits(this.resultado) ^ (Double.doubleToLongBits(this.resultado) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jogo)) {
            return false;
        }
        Jogo other = (Jogo) object;
        if ((this.idjogo == null && other.idjogo != null) || (this.idjogo != null && !this.idjogo.equals(other.idjogo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.edu.vianna.plus.calculatorplus.domain.Jogo[ idjogo=" + idjogo + " ]";
    }

    boolean estaCerto() {
        return resposta == resultado;
    }

    private double bonusUsuario() {
        final long tot = new JogoDao().getAllAcertos(partida.getUsuario().getId());
        double perc = (tot <= 10) ? 0 : (tot <= 50) ? 0.1 : (tot <= 100) ? 0.2 : (tot <= 150) ? 0.3 : (tot <= 200) ? 0.5 : 0.8;
        return perc;
    }

    private double bonusOperacao() {
        if (operador == EOperator.soma) {
            return 0.1;
        } else if (operador == EOperator.divisao) {
            return 0.9;
        }
        if (operador == EOperator.multiplicacao) {
            return 0.5;
        }
        if (operador == EOperator.subtracao) {
            return 0.2;
        }
        return 0;
    }

    public BigDecimal bonusInicial() {
        Random rand = new Random(new Date().getTime());
        double valor = rand.nextDouble();
        valor *= 9;
        valor += bonusOperacao();
        valor *= (1 + bonusUsuario());
        return Util.truncateDecimal(valor, 2);
    }

    public boolean isCorrect() {
        return resultado == resposta;
    }

}
