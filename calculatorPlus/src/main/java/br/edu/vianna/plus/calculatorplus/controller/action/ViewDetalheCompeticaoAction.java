/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.action;

import br.edu.vianna.plus.calculatorplus.controller.faces.IActionCommand;
import br.edu.vianna.plus.calculatorplus.dao.JogoDao;
import br.edu.vianna.plus.calculatorplus.dao.PartidaDao;
import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.domain.Jogo;
import br.edu.vianna.plus.calculatorplus.domain.Partida;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import br.edu.vianna.plus.calculatorplus.util.Util;
import java.time.LocalTime;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daves
 */
public class ViewDetalheCompeticaoAction implements IActionCommand {

    public ViewDetalheCompeticaoAction() {
    }

    @Override
    public boolean ehSegura() {
        return true;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        if (request.getParameter("id") != null) {
            try {
                int idPartida = Integer.parseInt(request.getParameter("id"));
                Usuario user = (Usuario) request.getSession().getAttribute("user");
                final PartidaDao pDao = new PartidaDao();
                Partida p = pDao.getPartida(idPartida);
                if (!p.getUsuario().getId().equals(user.getId())) {
                    request.setAttribute("msgErro", "Erro ::  a partida solicitada não lhe pertence" );
                    new ViewCompeticaoAction().execute(request, response);
                    return;
                }

                request.setAttribute("bonus", p.getBonificacao());
                request.setAttribute("partida", p);
                request.setAttribute("status", "view");
                RequestDispatcher rd = request.getRequestDispatcher("/template.jsp?page=resultado");
                rd.forward(request, response);

            } catch (Exception e) {
                request.setAttribute("msgErro", "Erro ::  " + e.getMessage());
                new ViewCompeticaoAction().execute(request, response);
            }
        } else {
            new ViewCompeticaoAction().execute(request, response);
        }

    }

}
