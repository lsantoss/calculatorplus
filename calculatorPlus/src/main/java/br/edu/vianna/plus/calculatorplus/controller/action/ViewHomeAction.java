/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.action;

import br.edu.vianna.plus.calculatorplus.controller.faces.IActionCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aluno
 */
public class ViewHomeAction implements IActionCommand {
    
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {        
        RequestDispatcher rd = request.getRequestDispatcher("/template.jsp");
        
        rd.forward(request, response);        
    }
    
    @Override
    public boolean ehSegura() {
      return false;    
    }
}
