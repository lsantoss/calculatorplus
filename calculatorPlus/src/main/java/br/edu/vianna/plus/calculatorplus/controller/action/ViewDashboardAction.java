/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.action;

import br.edu.vianna.plus.calculatorplus.controller.faces.IActionCommand;
import br.edu.vianna.plus.calculatorplus.dao.JogoDao;
import br.edu.vianna.plus.calculatorplus.dao.PartidaDao;
import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.util.Util;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aluno
 */
public class ViewDashboardAction implements IActionCommand {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        RequestDispatcher rd = request.getRequestDispatcher("/template.jsp?page=dashboard");
        
        request.setAttribute("numeroUsers", new UsuarioDao().getAllUsers());
        request.setAttribute("premiacaoUsers", Util.formatarMoeda( new PartidaDao().getAllBonus() ));
        request.setAttribute("numeroAcertos",  new JogoDao().getAllAcertos());
        request.setAttribute("numeroErros", Util.formatEmK(new JogoDao().getAllErros() ) );

        rd.forward(request, response);
    }

    @Override
    public boolean ehSegura() {
      return false;    
    }
}
