/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.action;

import br.edu.vianna.plus.calculatorplus.controller.faces.IActionCommand;
import br.edu.vianna.plus.calculatorplus.dao.JogoDao;
import br.edu.vianna.plus.calculatorplus.dao.PartidaDao;
import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.domain.Jogo;
import br.edu.vianna.plus.calculatorplus.domain.Partida;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import br.edu.vianna.plus.calculatorplus.util.Util;
import java.time.LocalTime;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daves
 */
public class ViewCompeticaoAction implements IActionCommand {

    public ViewCompeticaoAction() {
    }

    @Override
    public boolean ehSegura() {
        return true;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Usuario user = (Usuario) request.getSession().getAttribute("user");
        final PartidaDao uDao = new PartidaDao();
        final JogoDao jDao = new JogoDao();
        if (request.getParameter("op") != null) {
            if (request.getParameter("op").equals("new") && uDao.userJaCompetiuHoje(user)) {
                request.setAttribute("msgAviso", "Já Participous da competição hoje!!!");
                request.setAttribute("competicoes", uDao.getMinhasPartidas(user));
                RequestDispatcher rd = request.getRequestDispatcher("/template.jsp?page=listCompeticao");
                rd.forward(request, response);
            } else {
                Partida p;
                if (request.getParameter("op").equals("new")) {
                    p = uDao.iniciarPartida(user);
                    request.getSession().setAttribute("jogo", p);
                    request.getSession().setAttribute("time", LocalTime.now());
                } else {
                    p = (Partida) request.getSession().getAttribute("jogo");//uDao.getPartidaIniciada(user);
                }
                ArrayList<Integer> lista = new ArrayList<Integer>();
                if (request.getSession().getAttribute("jogos") == null) {
                    request.getSession().setAttribute("jogos", lista);
                } else {
                    lista = (ArrayList<Integer>) request.getSession().getAttribute("jogos");
                }

                if (request.getParameter("cpResposta") != null) {
                    try {
                        double valor = Double.parseDouble(request.getParameter("cpResposta"));
                        int id = Integer.parseInt(request.getParameter("idjogo"));
                        p = jDao.saveRespostaUser(id, valor);
                        request.getSession().setAttribute("jogo", p);
                    } catch (Exception e) {
                        request.setAttribute("msgAviso", "Erro ao Salvar a resposta, confira o valor!!! " + e.getMessage());
                        lista.remove(lista.size() - 1);
                    }
                }

                int idx = 0;
                Jogo jog = null;
                do {
                    jog = p.getJogoList().get(idx);
                    if (lista.indexOf(jog.hashCode()) == -1) {
                        lista.add(jog.hashCode());
                        break;
                    }
                    idx++;
                    if (idx == 10) {
                        //save
                        break;
                    }
                } while (true);

                if (idx == 10) {
                    final LocalTime inicio = (LocalTime) request.getSession().getAttribute("time");
                    request.setAttribute("bonus", p.getBonificacao());
                    p.setTempo(Util.tempoEmMinutos(inicio, LocalTime.now()));
                    if (p.getAcertos() == p.getJogoList().size()) {
                        p.setBonificacao(p.getBonificacao() * 2);
                    }
                    p = new PartidaDao().save(p);

                    request.setAttribute("msgAviso", "Parabéns pela participação!!! ");
                    request.setAttribute("partida", p);
                    request.setAttribute("tempoInicio", inicio);
                    request.setAttribute("tempoFim", LocalTime.now());
                    request.getSession().removeAttribute("jogo");
                    request.getSession().removeAttribute("jogos");
                    request.getSession().removeAttribute("time");
                    RequestDispatcher rd = request.getRequestDispatcher("/template.jsp?page=resultado");
                    rd.forward(request, response);
                } else {
                    request.getSession().setAttribute("jogos", lista);

                    request.setAttribute("partida", p);
                    request.setAttribute("perg", idx + 1);
                    request.setAttribute("total", p.getJogoList().size());
                    request.setAttribute("jogo", jog);
                    request.setAttribute("bonus", jog.getBonus());
                    RequestDispatcher rd = request.getRequestDispatcher("/template.jsp?page=jogar");
                    rd.forward(request, response);
                }
            }
        } else {
            request.setAttribute("competicoes", uDao.getMinhasPartidas(user));
            RequestDispatcher rd = request.getRequestDispatcher("/template.jsp?page=listCompeticao");
            rd.forward(request, response);
        }

    }

}
