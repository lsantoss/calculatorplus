package br.edu.vianna.plus.calculatorplus.domain;
import br.edu.vianna.plus.calculatorplus.dao.JogoDao;
import br.edu.vianna.plus.calculatorplus.util.Util;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class JogoIT {
    
    public JogoIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void estaCerto() {
        Jogo j = new Jogo(1, 10, 15, EOperator.soma, 25, 25, 1);
        boolean resultObtido = j.estaCerto();
        assertTrue("Ok", resultObtido);
    }
    
    @Test
    public void estaErrado() {
        Jogo j = new Jogo(1, 10, 15, EOperator.soma, 25, 10, 1);
        boolean resultObtido = j.estaCerto();
        assertFalse("Ok", resultObtido);
    }

    @Test
    public void isCorrect() {
        Jogo j = new Jogo(1, 10, 15, EOperator.soma, 25, 25, 1);
        boolean resultObtido = j.estaCerto();
        assertTrue("Ok", resultObtido);
    }
    
    @Test
    public void isFail() {
        Jogo j = new Jogo(1, 10, 15, EOperator.soma, 25, 10, 1);
        boolean resultObtido = j.estaCerto();
        assertFalse("Ok", resultObtido);
    }
    
    @Test
    public void bonusUsuario() {
        final long tot = 8;
        double perc = (tot <= 10) ? 0 : (tot <= 50) ? 0.1 : (tot <= 100) ? 0.2 : (tot <= 150) ? 0.3 : (tot <= 200) ? 0.5 : 0.8;
        double resultEsperado = 0;
        double resultObtido = perc;
        assertEquals(resultEsperado, resultObtido, 0);
    }
    
    @Test
    public void bonusUsuarioFalha() {
        final long tot = 8;
        double perc = (tot <= 10) ? 0 : (tot <= 50) ? 0.1 : (tot <= 100) ? 0.2 : (tot <= 150) ? 0.3 : (tot <= 200) ? 0.5 : 0.8;
        double resultEsperado = 0.8;
        double resultObtido = perc;
        assertNotEquals(resultEsperado, resultObtido, 0);
    }
}