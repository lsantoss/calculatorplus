package br.edu.vianna.plus.calculatorplus.domain;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class UsuarioIT {
    
    public UsuarioIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void notEquals() {
        Usuario u1 = new Usuario(1,"Gustin","gustin123","gustin@gmail.com","Minhasenha123@","Juiz de Fora",new Date());
        Usuario u2 = new Usuario(2,"Zezin","zezin123","ze@gmail.com","Minhasenha123@","Juiz de Fora",new Date());
        boolean resultEsperado = false;
        boolean resultObtido = u1.equals(u2);        
        assertEquals(resultEsperado, resultObtido);
    }
    
    @Test
    public void equals() {
        Usuario u1 = new Usuario(1,"Gustin","gustin123","gustin@gmail.com","Minhasenha123@","Juiz de Fora",new Date());
        Usuario u2 = new Usuario(1,"Gustin","gustin123","gustin@gmail.com","Minhasenha123@","Juiz de Fora",new Date());
        boolean resultEsperado = true;
        boolean resultObtido = u1.equals(u2);        
        assertEquals(resultEsperado, resultObtido);
    }
    
    @Test
    public void senhaValida() {
        Usuario u1 = new Usuario(1,"Gustin","gustin123","gustin@gmail.com","Minhasenha123@","Juiz de Fora",new Date());
        boolean resultEsperado = true;
        boolean resultObtido = u1.senhaValida();        
        assertEquals(resultEsperado, resultObtido);
    }
    
    @Test
    public void senhaInvalida() {
        Usuario u1 = new Usuario(1,"Gustin","gustin123","gustin@gmail.com","minhasenh","Juiz de Fora",new Date());
        boolean resultEsperado = false;
        boolean resultObtido = u1.senhaValida();        
        assertEquals(resultEsperado, resultObtido);
    }

    @Test
    public void loginValida() {
        Usuario u1 = new Usuario(1,"Gustin","gustin123","gustin@gmail.com","minhasenha","Juiz de Fora",new Date());
        boolean resultEsperado = true;
        boolean resultObtido = u1.loginValida();        
        assertEquals(resultEsperado, resultObtido);
    }   
    
    @Test
    public void loginInvalido() {
        Usuario u1 = new Usuario(1,"Gustin","gu123","gustin@gmail.com","minhasenha","Juiz de Fora",new Date());
        boolean resultEsperado = false;
        boolean resultObtido = u1.loginValida();        
        assertEquals(resultEsperado, resultObtido);
    } 
}