/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.dao;

import br.edu.vianna.plus.calculatorplus.controller.action.dto.RankingDTO;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import db.util.DbUnitHelper;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Rômulo
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UsuarioDaoTest {
    
    public UsuarioDaoTest() {
    }
    
    private static DbUnitHelper helper;
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Antes de Rodar todos os teste");
        helper = new DbUnitHelper("src/test/java/xml/baseTeste.xml");
        //helper.importar("test/xml/baseTeste.xml");
    }
    
    @AfterClass
    public static void tearDownClass() {  
        System.out.println("Depois de Rodar todos os teste");      
        helper.importar("src/test/java/xml/Bkb.xml");
    }
    
    @Before
    public void setUp() {
        System.out.println("Antes de Rodar um teste");
         helper.importar("src/test/java/xml/baseTeste.xml");
    }
    
    @After
    public void tearDown() {
        System.out.println("Depois de Rodar um teste");
    }
    
    @Test
    public void testSomeMethod1() {
        System.out.println("Verificando se a função getAllUsers retorna o número total de usuários");        
        long u = new UsuarioDao().getAllUsers();
        
        assertEquals(3, u);
    }
    
    @Test
    public void testSomeMethod2() {
        System.out.println("Verificando se a função getAllUsers retorna o número total de usuários");        
        long u = new UsuarioDao().getAllUsers();
        
        assertNotEquals(2, u);
    }
    
    @Test
    public void testSomeMethod3() {
        System.out.println("Verificando se a função findByLoginAndSenha retorna o usuário esperado ao passar login e senha");        
        Usuario user = new UsuarioDao().findByLoginAndSenha("ze", "123");
        boolean teste;
        
        if (user == null) {
            teste = false;
        } else {           
            teste = true;

        }
        
        Assert.assertTrue(teste);
    }
    
    @Test
    public void testSomeMethod4() {
        System.out.println("Verificando se a função findByLoginAndSenha retorna o usuário esperado ao passar login e senha");        
        Usuario user = new UsuarioDao().findByLoginAndSenha("zes", "123");
        boolean teste;
        
        if (user == null) {
            teste = false;
        } else {           
            teste = true;

        }
        
        Assert.assertFalse(teste);
    }
    
    
    @Test
    public void testSomeMethod5() {
        System.out.println("Verificando se a função getRanking retorna um ranking com todos os usuários de maneira descendente com base na soma das bonificações das partidas");        
        List<RankingDTO> list = new UsuarioDao().getRanking();
        boolean teste;
        
        if(list.get(0).getBonus() == 11.0 && list.get(1).getTempo() == 60 && list.get(2).getBonus() == 1){
            teste = true;
        }else{
            teste = false;
        }
        Assert.assertTrue(teste);        
    }
    
}