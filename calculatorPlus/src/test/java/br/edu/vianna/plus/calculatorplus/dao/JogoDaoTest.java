/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.dao;

import br.edu.vianna.plus.calculatorplus.domain.Partida;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import db.util.DbUnitHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Rômulo
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JogoDaoTest {
    
    public JogoDaoTest() {
    }
    
    private static DbUnitHelper helper;
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Antes de Rodar todos os teste");
        helper = new DbUnitHelper("src/test/java/xml/baseTeste.xml");
        //helper.importar("test/xml/baseTeste.xml");
    }
    
    @AfterClass
    public static void tearDownClass() {  
        System.out.println("Depois de Rodar todos os teste");      
        helper.importar("src/test/java/xml/Bkb.xml");
    }
    
    @Before
    public void setUp() {
        System.out.println("Antes de Rodar um teste");
         helper.importar("src/test/java/xml/baseTeste.xml");
    }
    
    @After
    public void tearDown() {
        System.out.println("Depois de Rodar um teste");
    }
    
    @Test
    public void testSomeMethod1() {
        System.out.println("Verificando se a função getAllAcertos retorna todos os acertos");        
        long a = new JogoDao().getAllAcertos();
        
        assertEquals(3, a);
    }
    
    @Test
    public void testSomeMethod2() {
        System.out.println("Verificando se a função getAllAcertos retorna todos os acertos");        
        long a = new JogoDao().getAllAcertos();
        
        assertNotEquals(5, a);
    }
    
    @Test
    public void testSomeMethod3() {
        System.out.println("Verificando se a função getAllErros retorna todos os erros");        
        long e = new JogoDao().getAllErros();
        
        assertEquals(2, e);
    }
    
    @Test
    public void testSomeMethod4() {
        System.out.println("Verificando se a função getAllErros retorna todos os erros");        
        long e = new JogoDao().getAllErros();
        
        assertNotEquals(4, e);
    }
    
    @Test
    public void testSomeMethod5() {
        System.out.println("Verificando se a função getAcertosPorDia retorna um vetor discriminando os valores referente ao número de acertos nos ultimos 10 dias");        
        long[] a = new JogoDao().getAcertosPorDia();
        
        boolean teste;
        
        if(a[9] == 2 && a[7] == 1){
            teste = true;
        }else {
            teste = false;
        }
        
        Assert.assertTrue(teste);
    }
    
    @Test
    public void testSomeMethod6() {
        System.out.println("Verificando se a função getErrosPorDia retorna um vetor discriminando os valores referente ao número de erros nos ultimos 10 dias");       
        long[] a = new JogoDao().getErrosPorDia();
        
        boolean teste;
        
        if(a[9] == 1 && a[7] == 1){
            teste = false;
        }else {
            teste = true;
        }
        
        assertFalse("Ok", teste);
    }
    
    @Test
    public void testSomeMethod7() {
        System.out.println("Verificando se a função getAllAcertos retorna o resultado esperado quando se passa um id");
        long r = new JogoDao().getAllAcertos(1);
        
        assertEquals(3, r);
    }
    
}