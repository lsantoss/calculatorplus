/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.dao;

import br.edu.vianna.plus.calculatorplus.domain.Jogo;
import br.edu.vianna.plus.calculatorplus.domain.Partida;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import db.util.DbUnitHelper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Rômulo
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PartidaDaoTest {
    
    public PartidaDaoTest() {
    }
    
    private static DbUnitHelper helper;
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Antes de Rodar todos os teste");
        helper = new DbUnitHelper("src/test/java/xml/baseTeste.xml");
        //helper.importar("test/xml/baseTeste.xml");
    }
    
    @AfterClass
    public static void tearDownClass() {  
        System.out.println("Depois de Rodar todos os teste");      
        helper.importar("src/test/java/xml/Bkb.xml");
    }
    
    @Before
    public void setUp() {
        System.out.println("Antes de Rodar um teste");
         helper.importar("src/test/java/xml/baseTeste.xml");
    }
    
    @After
    public void tearDown() {
        System.out.println("Depois de Rodar um teste");
    }
    
    @Test
    public void testSomeMethod1() {
        System.out.println("Verificando se a função getAllBonus retorna a soma da bonificação de todas as partidas");        
        double d = new PartidaDao().getAllBonus();
        
        assertEquals(15.0, d, 0);
    }
    
    @Test
    public void testSomeMethod2() {
        System.out.println("Verificando se a função getAllBonus retorna a soma da bonificação de todas as partidas");        
        double d = new PartidaDao().getAllBonus();
        
        assertNotEquals(90.0, d, 0);
    }
    
    @Test
    public void testSomeMethod3() {
        System.out.println("Verificando se a função getMinhasPartidas retorna a lista de partidas que um usuário participou de forma descendente");
        Usuario user = mock(Usuario.class);
        when(user.getId()).thenReturn(1);      
        List<Partida> plist = new PartidaDao().getMinhasPartidas(user);
        boolean teste;
        
        if(plist.get(0).getId() == 5 && plist.get(1).getId() == 2 && plist.get(2).getId() == 3 ){
            teste = true;
        }else{
            teste = false;
        }
        
        assertTrue("Ok", teste);
    }
    
    @Test
    public void testSomeMethod4() {
        System.out.println("Verificando se a função getMinhasPartidas retorna a lista de partidas que um usuário participou");
        Usuario user = mock(Usuario.class);
        when(user.getId()).thenReturn(2);       
        List<Partida> plist = new PartidaDao().getMinhasPartidas(user);
        
        assertNotEquals(2, plist.size());
    }
    
    @Test
    public void testSomeMethod5() {
        System.out.println("Verificando se a função userJaCompetiuHoje retorna se um determinado usuário já teve alguma partida no dia atual");
        Usuario user = mock(Usuario.class);
        when(user.getId()).thenReturn(2);        
        boolean teste = new PartidaDao().userJaCompetiuHoje(user);
        
        assertTrue("Ok", teste);
    }
    
    @Test
    public void testSomeMethod6() {
        System.out.println("Verificando se a função userJaCompetiuHoje retorna se um determinado usuário já teve alguma partida no dia atual");
        Usuario user = mock(Usuario.class);
        when(user.getId()).thenReturn(3);        
        boolean teste = new PartidaDao().userJaCompetiuHoje(user);
        
        assertFalse("Ok", teste);
    }
    
    @Test
    public void testSomeMethod7() throws SQLException {
        System.out.println("Verificando se a função iniciarPartida cria uma partida com 10 jogos");
        
        Usuario user = new UsuarioDao().findByLoginAndSenha("ze", "123"); 
        
        Partida part = new PartidaDao().iniciarPartida(user);
        
        boolean teste;
        
        if(part.getJogoList().size() == 10){
            teste = true;
        } else {
            teste = false;
        }
        
        assertTrue("Ok", teste);
    }
    
    @Test
    public void testSomeMethod8() throws SQLException {
        System.out.println("Verificando se a função getPartidaIniciada retorna a primeira partida iniciada por determinado usuário no dia atual");
        Usuario user = mock(Usuario.class);
        when(user.getId()).thenReturn(1);  
        
        Partida part = new PartidaDao().getPartidaIniciada(user);
        
        boolean teste;
        
        if(part.getId() == 2){
            teste = true;
        } else {
            teste = false;
        }
        
        assertTrue("Ok", teste);
    }
    
    @Test
    public void testSomeMethod9() throws SQLException {
        System.out.println("Verificando se a função getPartidaIniciada retorna null se o usuario não tiver participado de nenhum partida no dia atual");
        Usuario user = mock(Usuario.class);
        when(user.getId()).thenReturn(3);  
        
        Partida part = new PartidaDao().getPartidaIniciada(user);
        
        boolean teste;
        
        if(part == null){
            teste = false;
        } else {
            teste = true;
        }
        
        assertFalse("Ok", teste);
    }
    
    
}