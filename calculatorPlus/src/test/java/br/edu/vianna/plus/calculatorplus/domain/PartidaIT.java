package br.edu.vianna.plus.calculatorplus.domain;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class PartidaIT {
    
    public PartidaIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void getAcertos() {
        ArrayList<Jogo> listajogos = new ArrayList();
        listajogos.add(new Jogo(123,5,5,EOperator.soma,10,10,5));
        listajogos.add(new Jogo(125,5,5,EOperator.soma,10,4,5));
        listajogos.add(new Jogo(128,15,5,EOperator.soma,20,10,5));
        listajogos.add(new Jogo(127,25,5,EOperator.soma,30,30,5));
        listajogos.add(new Jogo(121,55,5,EOperator.soma,60,60,5));
        Partida p = new Partida(100,new Date(),10.0,3600);
        
        p.setJogoList(listajogos);
        
        int resultEsperado = 3;
        int resultObtido = p.getAcertos();
        assertEquals(resultEsperado, resultObtido);
    }
    
    @Test
    public void getAcertosFalha() {
        ArrayList<Jogo> listajogos = new ArrayList();
        listajogos.add(new Jogo(123,5,5,EOperator.soma,10,10,5));
        listajogos.add(new Jogo(125,5,5,EOperator.soma,10,4,5));
        listajogos.add(new Jogo(128,15,5,EOperator.soma,20,10,5));
        listajogos.add(new Jogo(127,25,5,EOperator.soma,30,30,5));
        listajogos.add(new Jogo(121,55,5,EOperator.soma,60,60,5));
        Partida p = new Partida(100,new Date(),10.0,3600);
        
        p.setJogoList(listajogos);
        
        int resultEsperado = 6;
        int resultObtido = p.getAcertos();
        assertNotEquals(resultEsperado, resultObtido);
    }

    @Test
    public void getErros() {
        ArrayList<Jogo> listajogos = new ArrayList();
        listajogos.add(new Jogo(123,5,5,EOperator.soma,10,10,5));
        listajogos.add(new Jogo(125,5,5,EOperator.soma,10,4,5));
        listajogos.add(new Jogo(128,15,5,EOperator.soma,20,10,5));
        listajogos.add(new Jogo(127,25,5,EOperator.soma,30,30,5));
        listajogos.add(new Jogo(121,55,5,EOperator.soma,60,60,5));
        Partida p = new Partida(100,new Date(),10.0,3600);
        
        p.setJogoList(listajogos);
        
        int resultEsperado = 2;
        int resultObtido = p.getErros();
        assertEquals(resultEsperado, resultObtido);
    }
    
    @Test
    public void getErrosFalha() {
        ArrayList<Jogo> listajogos = new ArrayList();
        listajogos.add(new Jogo(123,5,5,EOperator.soma,10,10,5));
        listajogos.add(new Jogo(125,5,5,EOperator.soma,10,4,5));
        listajogos.add(new Jogo(128,15,5,EOperator.soma,20,10,5));
        listajogos.add(new Jogo(127,25,5,EOperator.soma,30,30,5));
        listajogos.add(new Jogo(121,55,5,EOperator.soma,60,60,5));
        Partida p = new Partida(100,new Date(),10.0,3600);
        
        p.setJogoList(listajogos);
        
        int resultEsperado = 12;
        int resultObtido = p.getErros();
        assertNotEquals(resultEsperado, resultObtido);
    }
    
    @Test
    public void addBonus() {
        Partida p = new Partida(100,new Date(),10.0,3600);
        p.addBonus(5.0);
        double resultEsperado = 15.0;
        double resultObtido = p.getBonificacao();
        assertEquals(resultEsperado, resultObtido,0);
    }
    
    @Test
    public void addBonusFalha() {
        Partida p = new Partida(100,new Date(),10.0,3600);
        p.addBonus(5.0);
        
        double resultEsperado = 15.0;
        double resultObtido = p.getBonificacao()+3;
        boolean teste = true;
        
        if(resultEsperado != resultObtido){
            teste = false;
        }
        assertFalse("Ok", teste);
    }

    @Test
    public void removeBonus() {
        Partida p = new Partida(100,new Date(),10.0,3600);
        p.removeBonus(5.0);
        double resultEsperado = 5.0;
        double resultObtido = p.getBonificacao();
        assertEquals(resultEsperado, resultObtido,0);
    }    
    
    @Test
    public void removeBonusFalha() {
        Partida p = new Partida(100,new Date(),10.0,3600);
        p.removeBonus(5.0);
        double resultEsperado = 5.0;
        double resultObtido = p.getBonificacao()+3;
        boolean teste = true;
        
        if(resultEsperado != resultObtido){
            teste = false;
        }
        assertFalse("Ok", teste);
    }  
}