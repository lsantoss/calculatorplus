/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.domain.mock;


import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import java.util.Date;
import org.junit.Assert;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class UsuarioMockTest {
    
    
    @Test
    public void usuarioLogado(){
        
        Usuario u1 = new Usuario(1,"Gustin","gustin123","gustin@gmail.com","Minhasenha123@","Juiz de Fora",new Date());
        UsuarioDao uDao = mock(UsuarioDao.class);
    
        when(uDao.findByLoginAndSenha("gustin123", "Minhasenha123@")).thenReturn(u1);
        
        boolean achou = false;
        
        Usuario userLogado;
        userLogado = uDao.findByLoginAndSenha("gustin123", "Minhasenha123@");
        
        if(userLogado != null){
            achou = true;
        }
        
        Assert.assertTrue("Usuário Logado com sucesso!", achou);
    }
    
    @Test
    public void usuarioLogadoFalha(){
        
        Usuario u1 = new Usuario(1,"Gustin","gustin123","gustin@gmail.com","Minhasenha123@","Juiz de Fora",new Date());
        UsuarioDao uDao = mock(UsuarioDao.class);
    
        when(uDao.findByLoginAndSenha("gustin123", "Minhasenha123@")).thenReturn(u1);
        
        boolean achou = false;
        
        Usuario userLogado;
        userLogado = uDao.findByLoginAndSenha("gustin", "Minhasenha123@");
        
        if(userLogado != null){
            achou = true;
        }
        
        Assert.assertFalse("Usuário e/ou senha incorreto(s)!!", achou);
    }
    
}
